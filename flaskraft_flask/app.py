from flask import Flask
from sqlalchemy import create_engine

app = Flask(__name__)

engine = create_engine('sqlite:///db.sqlite3', echo=True)
