from typing import List

from sqlalchemy import Boolean, Column, ForeignKey, Integer, SmallInteger, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, Session

from flaskraft_flask import engine

Base = declarative_base()


class Block(Base):
    __tablename__ = 'block'

    id = Column(Integer, primary_key=True)
    name = Column(String(35), unique=True)
    img = Column(String(50), unique=True)
    img_3d = Column(String(50), unique=True)
    hardness = Column(SmallInteger)
    stack_size = Column(SmallInteger)
    diggable = Column(Boolean)
    transparent = Column(Boolean)
    luminance = Column(SmallInteger)

    def __repr__(self):
        return "<Block {}>".format(self.id)

    def __str__(self):
        return self.name

    @staticmethod
    def get_blocks():
        session = Session(bind=engine)
        res = session.query(Block).all()
        session.close()
        return res

    @staticmethod
    def get_block(block_id: int):
        session = Session(bind=engine)
        res = session.query(Block).get(block_id)
        session.close()
        return res

    @staticmethod
    def search_blocks(q: str):
        session = Session(bind=engine)
        res = session.query(Block).filter(Block.name.like('%' + q + '%'))
        session.close()
        return res

    @staticmethod
    def get_craft(block_id: int):
        session = Session(bind=engine)
        res = session.query(Block.name, Block.img).join(Ingredient.block_relationship).filter(
            Ingredient.crafted_block_id == block_id).all()
        session.close()
        return res

    @staticmethod
    def add_craft(block_id: int, ingredients: List[int]) -> bool:
        len_ingredients = len(ingredients)
        if len_ingredients == 4 or len_ingredients == 9:
            session = Session(bind=engine)
            session.add_all([Ingredient(crafted_block_id=block_id, position=x + 1, block_id=ingredients[x])
                             for x in range(len_ingredients)])
            session.commit()
            session.close()
            return True
        else:
            return False


class Ingredient(Base):
    __tablename__ = 'ingredient'

    crafted_block_id = Column(Integer, ForeignKey('block.id'), primary_key=True)
    position = Column(SmallInteger, primary_key=True)
    block_id = Column(Integer, ForeignKey('block.id'))

    block_relationship = relationship('Block', foreign_keys='Ingredient.block_id')


class Item(Base):
    __tablename__ = 'item'

    id = Column(Integer, primary_key=True)
    name = Column(String(35), unique=True)
    img = Column(String(50), unique=True)
    stack_size = Column(SmallInteger)

    def __repr__(self):
        return "<Item {}>".format(self.id)

    def __str__(self):
        return self.name

    @staticmethod
    def get_items():
        session = Session(bind=engine)
        res = session.query(Item).all()
        session.close()
        return res

    @staticmethod
    def get_item(item_id: int):
        session = Session(bind=engine)
        res = session.query(Item).get(item_id)
        session.close()
        return res

    @staticmethod
    def search_items(q: str):
        session = Session(bind=engine)
        res = session.query(Item).filter(Item.name.like('%' + q + '%'))
        session.close()
        return res
