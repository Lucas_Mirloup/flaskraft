import yaml
from sqlalchemy.orm import Session

from .app import app, engine
from .models import Base, Block, Item


@app.cli.command()
def loaddb():
    """Creates the tables and populates them with data"""
    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)

    yml = yaml.load(open('flaskraft_flask/static/blocks.yaml'))

    session = Session(bind=engine)
    for b in yml:
        o = Block(id=b['id'], name=b['displayName'], img='img/blocks/' + b['name'] + '.png',
                  img_3d='img/blocks_3D/' + str(b['id']) + '-0.png', hardness=b['hardness'],
                  stack_size=b['stackSize'], diggable=b['diggable'],
                  transparent=b['transparent'], luminance=b['emitLight'])
        session.add(o)

    yml2 = yaml.load(open('flaskraft_flask/static/items.yaml'))
    for i in yml2:
        o = Item(id=i['id'], name=i['displayName'], img='img/items/' + str(i['id']) + '-0.png',
                 stack_size=i['stackSize'])
        session.add(o)

    Block.add_craft(24, [12, 12, 12, 12])
    Block.add_craft(54, [5, 5, 5, 5, 0, 5, 5, 5, 5])
    session.commit()
    session.close()


@app.cli.command()
def syncdb():
    """Creates missing tables in the database."""
    Base.metadata.create_all(engine)


@app.cli.command()
def resetblocks():
    """Creates missing tables in the database."""
    Block.__table__.drop(engine)
    Base.metadata.create_all(engine)
