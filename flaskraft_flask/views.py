from flask import render_template, request, redirect, url_for

from .models import Block, Item
from .app import app


@app.route("/")
def home():
    return render_template("home.html")


@app.route("/blocks")
def blocks():
    return render_template("blocks.html", blocks=Block.get_blocks())


@app.route("/items")
def items():
    return render_template("items.html", items=Item.get_items())


@app.route("/block/<int:block_id>/")
@app.route("/block/", defaults={'block_id': None})
def block(block_id: int):
    if block_id is None:
        return redirect(url_for('blocks'))
    craft = Block.get_craft(block_id)
    return render_template("block.html", block=Block.get_block(block_id), craft=craft, len_craft=len(craft))


@app.route("/item/<int:item_id>/")
@app.route("/item/", defaults={'item_id': None})
def item(item_id: int):
    if item_id is None:
        return redirect(url_for('items'))
    return render_template("item.html", item=Item.get_item(item_id))


@app.route("/search")
def search():
    return render_template("search.html", blocks=Block.search_blocks(request.args.get("q")),
                           items=Item.search_items(request.args.get("q")))


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404
